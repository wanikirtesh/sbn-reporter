# SBN Reports #

This is a simple project which implements **TestNG** Report listener and produces elegant reports with various features

### Maven Dependency ###

```
#!xml

<dependency>
  <groupId>org.bitbucket.wanikirtesh</groupId>
  <artifactId>sbnreporter_lite</artifactId>
  <version>1.0.1</version>
</dependency>
```

### How To Implement ###

+ There are two ways to implement SBN Reports in your project
    *   Using testng.xml file
    *    Adding @Listners in your test class

### Configuration (not mandatory) ###

+ Add Report configuration file to the root of your project

Ex. reportconfig.properties, add below content to it
```
#!properties
sbn.reports.dir = Reports
sbn.proj.header = <<PROJECT Header show in the report>>
sbn.proj.description = <<PROJECT Description  to show in the report>>

```

You will need to either configure testng.xml or your test class as shown below

### Configuring testng.xml ###


```
#!xml

<!DOCTYPE suite SYSTEM "http://testng.org/testng-1.0.dtd" >

<suite name="TestAll">
    <listeners>
        <listener class-name="com.sbn.reports.listners.TestListener"></listener>
    </listeners>

```


### Configuring Test Class ###


```
#!java


@Listeners(com.sbn.reports.listners.TestListener.class)
public class MyTest {
```

### Add Author information in test ###

+ Add author information in test methods as below


```
#!java


SBNReporter.setAuthorInfo("Kirtesh Wani","01 Jan 2019","1.0");
```

### Split you test in logical steps using below methods ###